import React, {useEffect, useMemo} from 'react';
import 'antd/dist/antd.css';
import "./style.scss"
import {useDispatch, useSelector} from "react-redux";
import {
    fetchPersonalPage,
    fetchUserStats, fetchUserStatsMessages,
    selectPersonalPage,
    selectStats,
    selectMessagesStats, fetchUserStatsLikes, selectLikesStats
} from "../../../store/personal_page/personal_page.slice";
import {Avatar} from "../../common/avatar/avatar";
import {useLocation} from "react-router-dom";
import {Col, Row} from "antd";
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

export const PersonalArea = () => {

    const location = useLocation()
    const dispatch = useDispatch();
    const personal_info = useSelector(selectPersonalPage);
    const stats = useSelector(selectStats);
    const messagesStats = useSelector(selectMessagesStats);
    const likesStats = useSelector(selectLikesStats);

    useEffect(() => {
        let search_id = "me"
        if(location.search)
            search_id = location.search.slice(1)
        dispatch(fetchPersonalPage(search_id))
        dispatch(fetchUserStats(search_id))
        dispatch(fetchUserStatsMessages(search_id))
        dispatch(fetchUserStatsLikes(search_id))
    },[])

    const messagesStatsOptions = useMemo(() => ({
        title: {
            text: ''
        },
        xAxis: {
            categories: messagesStats.map((stat) => stat.label),
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        series: [{
            data: messagesStats.map((stat) => stat.count)
        }],
        chart: {
            type: 'column',
            height: '250px'
        },
        legend: {
            enabled: false
        }
    }), [messagesStats]);

    const likesStatsOptions = useMemo(() => ({
        title: {
            text: ''
        },
        xAxis: {
            categories: likesStats.map((stat) => stat.label),
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        series: [{
            data: likesStats.map((stat) => stat.count)
        }],
        chart: {
            type: 'column',
            height: '250px'
        },
        legend: {
            enabled: false
        }
    }), [likesStats]);

    return (
        <div className="personal-area">
            <div className="personal-area__container">
                <div className="personal-area__card personal-area__card_user">
                    <Row align="middle" style={{ minWidth: '600px'}}>
                        <Col span={8}>
                            <Avatar
                                width={150}
                                className="user__logo"
                                email={personal_info.email}
                            />
                        </Col>
                        <Col span={16}>
                            <h2 className="user__name">{personal_info.name}</h2>
                            <h2 className="user__email">{personal_info.email}</h2>
                            <p className="user__email">{personal_info.public_id}</p>
                        </Col>
                    </Row>
                    <Row>
                        <p className="user__description">{personal_info.description}</p>
                    </Row>
                </div>
                <div className="personal-area__card stat-card">
                    <h3 className="stat-card__title">
                        Отправленные сообщения
                    </h3>
                    <div className="stat-card__number">
                        {stats.sent_messages}
                    </div>
                </div>
                <div className="personal-area__card stat-card">
                    <h3 className="stat-card__title">
                        Полученные сообщения
                    </h3>
                    <div className="stat-card__number">
                        {stats.received_messages}
                    </div>
                </div>
                <div className="personal-area__card stat-card">
                    <h3 className="stat-card__title">
                        Лайков поставлено
                    </h3>
                    <div className="stat-card__number">
                        {stats.likes}
                    </div>
                </div>
                <div className="personal-area__card stat-card">
                    <h3 className="stat-card__title">
                        Подписки на сообщества
                    </h3>
                    <div className="stat-card__number">
                        {stats.subscriptions}
                    </div>
                </div>
                <div className="personal-area__card personal-area__card_messages messages-stats">
                    <h3 className="stat-card__title">
                        Статистика по сообщениям
                    </h3>
                    <div className="messages-stats__content">
                        <HighchartsReact
                            highcharts={Highcharts}
                            options={messagesStatsOptions}
                        />
                    </div>
                </div>
                <div className="personal-area__card personal-area__card_likes messages-stats">
                    <h3 className="stat-card__title">
                        Статистика по лайкам
                    </h3>
                    <div className="messages-stats__content">
                        <HighchartsReact
                            highcharts={Highcharts}
                            options={likesStatsOptions}
                        />
                    </div>
                </div>
            </div>
        </div>
    );

}
