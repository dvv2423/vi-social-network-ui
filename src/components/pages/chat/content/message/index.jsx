import React, {useMemo, useCallback, useState} from 'react';
import 'antd/dist/antd.css';
import "./style.scss"
import {Button} from 'antd';
import {useAudio} from "../../../../../common/hooks/audio";
import {PauseOutlined} from '@ant-design/icons'
import {API_URL} from "../../../../../common/constants/url";
import axios from "axios";
import moment from "moment";

export const Message = (message) => {
    const [play, playing, toggle] = useAudio();
    const [recognizedText, setRecognizedText] = useState('')

    const msg = message.message

    const voiceUrl = useMemo(() => {
        if (!msg.voice) {
            return '';
        }
        if (msg.voice.slice(0, 4) === 'blob') {
            return msg.voice;
        }
        return `${API_URL}/${msg.voice}`
    }, []);

    const handleRecognize = useCallback(() => {
        if (!msg.voice) {
            return;
        }
        axios.get(`${API_URL}/api/recognize-speech?url=${msg.voice}`, {
            headers: {
                'x-access-token': localStorage.getItem('token')
            }
        }).then((response) => {
            setRecognizedText(response.data)
        })
    }, [])

    return (
        <div className={msg.mine ? 'message__container_my' : 'message__container'}>
            <div
                className={`message__sender ${msg.mine && 'message__sender_my'}`}
            >
                {msg.name}
            </div>
            <div
                className={`message ${msg.mine && 'message_my'}`}
            >
                <p
                    className={`message__text ${msg.mine && 'message_text_my'}`}
                >
                    {msg.text}
                </p>
                {recognizedText && (
                    <p
                        className={`message__text ${msg.mine && 'message_text_my'}`}
                    >
                        {recognizedText}
                    </p>
                )}
                {voiceUrl && (
                    <>
                        {playing ? (
                            <Button
                                shape="circle"
                                size="large"
                                className="recordButton recordButtonStop"
                                onClick={toggle}
                            >
                                <PauseOutlined />
                            </Button>
                        ) : (
                            <Button
                                shape="circle"
                                size="large"
                                className="recordButton recordButtonStart"
                                onClick={() => { play(voiceUrl) }}
                            >
                                ▶︎
                            </Button>
                        )}

                        <Button
                            type="text"
                            className="recordButton recordButtonStart"
                            onClick={handleRecognize}
                        >
                            Recognize
                        </Button>
                    </>
                )}
            </div>
            <div
                className={`message__datetime ${msg.mine && 'message__datetime_my'}`}
            >
                {msg.datetime && moment(msg.datetime).local().format('DD.MM HH:mm')}
            </div>
        </div>
    );

}
