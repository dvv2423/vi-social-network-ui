import React, {useState} from 'react';
import 'antd/dist/antd.css';
import "./style.scss"
import {HeartOutlined} from "@ant-design/icons";
import {LikeAsync} from "../../../../../store/groupMessages/groupMessages.slice";
import {useDispatch} from "react-redux";
export const FeedElement = (state) => {


    const [i, setI] = React.useState(0)
    let states = state.state
    const dispatch = useDispatch();

    React.useEffect(() => {
        setI(0)
    },[states])
    const likePost = (public_id) => {
        console.log(1)
        dispatch(LikeAsync(public_id))
        setI(i + 1)
    }
    return (
        <div className="feed_element">
            <h3 className="feed_element_msg_author">{states.title}</h3>
            <div className="feed_element_msg_text">{states.text}</div>
            <div className="feed_element_likes">
                <HeartOutlined onClick={() => {likePost(states.public_id)}} className="like"/>
                <div >{states.likes + i}</div>
            </div>
        </div>
    );
}
