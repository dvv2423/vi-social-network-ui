import 'antd/dist/antd.css'
import './App.scss';
import { Page } from './components/page'
import { BrowserRouter } from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import {selectMyInfo} from "./store/auth/auth.slice";
import {useEffect} from "react";
import {socketConnect} from "./store/chat/chat.slice";

function App() {
    const dispatch = useDispatch()
    const me = useSelector(selectMyInfo);

    useEffect(() => {
        if (!me) {
            return;
        }
        dispatch(socketConnect())
    }, [me]);

    return (
      <BrowserRouter>
        <Page/>
      </BrowserRouter>
    );
}

export default App;
