import {createSelector, createSlice} from '@reduxjs/toolkit'
import axios from "axios";
import {API_URL} from "../../common/constants/url";
import {errorHandler} from "../../utils/error-handler";

const initialState = {
    personalPageLoading: false,
    personal_page: [],
    statsLoading: false,
    stats: {},
    messagesStatsLoading: false,
    messagesStats: [],
    likesStatsLoading: false,
    likesStats: []
}


//Reducer
export const personalPageSlice = createSlice({
    name: 'personal_page',
    initialState,
    reducers: {
        personalPageLoading(state, _action) {
            state.personalPageLoading = true
        },
        personalPageReceived(state, action) {
            state.personalPageLoading = false
            state.personal_page = action.payload
        },
        statsLoading(state, _action) {
            state.statsLoading = true
        },
        statsReceived(state, action) {
            state.statsLoading = false
            state.stats = action.payload
        },
        messagesStatsLoading(state, _action) {
            state.messagesStatsLoading = true
        },
        messagesStatsReceived(state, action) {
            state.messagesStatsLoading = false
            state.messagesStats = action.payload
        },
        likesStatsLoading(state, _action) {
            state.likesStatsLoading = true
        },
        likesStatsReceived(state, action) {
            state.likesStatsLoading = false
            state.likesStats = action.payload
        },
    }
})

// Actions
export const {
    personalPageLoading,
    personalPageReceived,
    statsLoading,
    statsReceived,
    messagesStatsLoading,
    messagesStatsReceived,
    likesStatsLoading,
    likesStatsReceived
} = personalPageSlice.actions


export const fetchPersonalPage = (search_id) => async (dispatch) => {
    dispatch(personalPageLoading())
    try {
        const response = await axios.get(`${API_URL}/api/users/${search_id}`, {
            headers: {
                'x-access-token': localStorage.getItem('token')
            }
        })
        dispatch(personalPageReceived(response.data))
    } catch (error) {
        errorHandler(error)
    }
}

export const fetchUserStats = (search_id) => async (dispatch) => {
    dispatch(statsLoading())
    try {
        const response = await axios.get(`${API_URL}/api/users/${search_id}/stats`, {
            headers: {
                'x-access-token': localStorage.getItem('token')
            }
        })
        dispatch(statsReceived(response.data))
    } catch (error) {
        errorHandler(error)
    }
}

export const fetchUserStatsMessages = (search_id) => async (dispatch) => {
    dispatch(messagesStatsLoading())
    try {
        const response = await axios.get(`${API_URL}/api/users/${search_id}/stats/messages`, {
            headers: {
                'x-access-token': localStorage.getItem('token')
            }
        })
        dispatch(messagesStatsReceived(response.data))
    } catch (error) {
        errorHandler(error)
    }
}

export const fetchUserStatsLikes = (search_id) => async (dispatch) => {
    dispatch(likesStatsLoading())
    try {
        const response = await axios.get(`${API_URL}/api/users/${search_id}/stats/likes`, {
            headers: {
                'x-access-token': localStorage.getItem('token')
            }
        })
        dispatch(likesStatsReceived(response.data))
    } catch (error) {
        errorHandler(error)
    }
}


// Selectors
const selectPersonalPageState = (state) => state.personal_page;
const selectPersonalPage = createSelector([selectPersonalPageState], personal_page =>
    personal_page.personal_page
);
const selectStats = createSelector([selectPersonalPageState], personal_page =>
    personal_page.stats
);
const selectMessagesStats = createSelector([selectPersonalPageState], personal_page =>
    personal_page.messagesStats
);
const selectLikesStats = createSelector([selectPersonalPageState], personal_page =>
    personal_page.likesStats
);

export { selectPersonalPageState, selectPersonalPage, selectStats, selectMessagesStats, selectLikesStats };

export default personalPageSlice.reducer
