export const ADDRESS = '127.0.0.1'

export const API_URL = `http://${ADDRESS}:5000`

export const IO_URL = `ws://${ADDRESS}:5000`

